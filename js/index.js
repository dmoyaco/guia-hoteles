$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({
	interval: 2000
  });
  
  $('#contacto').on('show.bs.modal', function(e) {
	console.log("El modal contacto se está mostrando");
	
	$('#btnContactoSevilla').prop('disabled', true);
	$('#btnContactoSevilla').removeClass('btn-success');
	$('#btnContactoSevilla').addClass('btn-secondary');
  });
  $('#contacto').on('shown.bs.modal', function(e) {
	console.log("El modal contacto mostró");
  });
  $('#contacto').on('hide.bs.modal', function(e) {
	console.log("El modal contacto se está ocultando");
  });
  $('#contacto').on('hidden.bs.modal', function(e) {
	console.log("El modal contacto se ocultó");
	
	$('#btnContactoSevilla').prop('disabled', false);
	$('#btnContactoSevilla').removeClass('btn-secondary');
	$('#btnContactoSevilla').addClass('btn-success');
  });
});